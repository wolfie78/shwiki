# shwiki

## Description
`shwiki` is a shell based wiki program which uses `newt` as a nice tui.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
### Pre-Reqs
Redhat based OS's - `$ sudo dnf install newt`

### Instalation
Clone the repo
```
$ git clone git@gitlab.com:wolfie78/shwiki.git
```

Then cd directory to the shwiki and then link / copy it to an area in your path (i.e. /bin)
```
$ cd shwiki
$ cp shwiki /usr/bin
```

## Usage
To run `shwiki` simply type `shwiki`
```
$ shwiki
```

## Curent problems
1. it will backup any file you view, not just if you modified it.
2. editor is set to vim, this will change.

## Support
if you have any problems please raise an issue at: https://gitlab.com/wolfie78/shwiki

## Roadmap
TBD

## Contributing
TBD

## Authors and acknowledgment
1. Alan Wolffs -- (https://gitlab.com/wolfie78/shwiki)
2. Newt -- (https://pagure.io/newt)

## License
```
Copyright 2024 Wolfie

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “shwiki”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```

## Project status
Currently in Development
